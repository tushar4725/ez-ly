import Errors from './Errors'

class Form {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    add(key, value) {
        this.originalData[key] = this[key] = value;
    }

    /**
     * Fetch all relevant data for the form.
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    /**
     * Fetch formdata while appending file.
     *
     * @param  File photo
     * @param  String name
     * @return FromData
     */
    dataWithFile(photo, name = 'photo') {
        let data = new FormData();

        _.forIn(this.data(), (value, key) => {
            data.append(key, this.nullsToEmptyStrings(value));
        });

        data.append(name, photo || '');

        return data;
    }


    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }

    /**
     * Returns null as empty string.
     *
     * @param  Object obj
     * @return string
     */
    nullsToEmptyStrings(obj) {
        return JSON.parse(JSON.stringify(obj, (k, v) => (v === null) ? '' : v));
    }
}

export default Form;
