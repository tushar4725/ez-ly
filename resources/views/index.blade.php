@extends('layouts.main')

@section('title', "Ez.ly")

@section('content')
<div class="content">
    <div class="title">
        <img class="brand-logo" src="{{ asset('img/logo.png') }}" width="140" height="90" alt="Ez.ly">
    </div>
    <p>Easily shorten your url</p>


    <div class="row padding-lr-50">
        <create-url-form
            route="{{ route('url.submit') }}"
            redirect-to="{{ route('index') }}"
            inline-template v-cloak>
            <form role="form" :action="route" method="POST" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)" class="col-md-12">
                <div class="form-group row mt-5">
                    <div class="col-sm-12">
                        <input type="text" v-model="form.url" name="url" placeholder="Enter URL" class="form-control">
                        <error-message>@{{ form.errors.get('url') }}</error-message>
                    </div>
                </div>
                <div class="form-group m-4">
					<div class="row text-center">
						<div class="col">
							<button-spinner
								type="button.submit"
								class="btn btn-outline-success"
								:is-loading="button.isSubmitted">
								<span>Save</span>
							</button-spinner>
						</div>
                    </div>
				</div>
                <error-message-box>@{{ errorMessage }}</error-message-box>
				<success-message-box>@{{ successMessage }}</success-message-box>
            </form>
        </create-url-form>

    </div>
</div>
@endsection

