<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title')</title>

		@include('includes.css')
	</head>
	<body class="app flex-row align-items-center">
		
		<div id="app" style="width:100%;">
			@yield('content')
		</div>
		
		@include('includes.js')
		
	
	</body>
</html>
