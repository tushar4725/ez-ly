<?php

namespace App\Services;

use App\Validators\UrlValidator;
use App\Repositories\Contracts\UrlRepository;
use App\Repositories\Contracts\HashRepository;

class UrlService
{
    protected $urls;
    protected $hashs;

	/**
	 * Url service constructor.
	 * 
	 */
    public function __construct(
		UrlRepository $urls,
		HashRepository $hashs
	){
        $this->urls = $urls;
        $this->hashs = $hashs;
	}

	/**
	 * Get url by value.
	 * 
	 */
	public function get($value)
	{
		return $this->urls->getWhere('value', $value, null, false)->first();
	}


	/**
	 * Get url by hash
	 */
	public function getUrl($hash)
	{
		$hash = $this->hashs->getWhere('value', $hash)->first();

		return $hash->url->value;

	}

	/**
	 * Create Shortened Url.
	 * 
	 */
	public function create($inputs, UrlValidator $validator)
	{
		$validator->fire($inputs, 'create');

		$url = $this->get(array_get($inputs, 'url'));
		
		if(isset($url)){
			return getCompleteShortenedUrl($url->hash->value);
		}

        $url = $this->urls->create([
			'value' => array_get($inputs, 'url')
		]);

		$urlHash = getUrlHash($url->id);

		$hash = $this->urls->createRelationally($url,'hash',[
			'value' => $urlHash
		]);

		return getCompleteShortenedUrl($url->hash->value);
	}

}