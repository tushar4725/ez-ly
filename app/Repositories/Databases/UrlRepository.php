<?php

namespace App\Repositories\Databases;

use App\Models\Url;
use App\Traits\DatabaseRepositoryTrait;
use App\Repositories\Contracts\UrlRepository as UrlRepositoryContract;

class UrlRepository implements UrlRepositoryContract
{
    use DatabaseRepositoryTrait;

    private $model = Url::class; 

}
