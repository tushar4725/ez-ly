<?php

namespace App\Repositories\Databases;

use App\Models\Hash;
use App\Traits\DatabaseRepositoryTrait;
use App\Repositories\Contracts\HashRepository as HashRepositoryContract;

class HashRepository implements HashRepositoryContract
{
    use DatabaseRepositoryTrait;

    private $model = Hash::class; 

}
