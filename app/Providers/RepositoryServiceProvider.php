<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Url
        $this->app->bind(
            \App\Repositories\Contracts\UrlRepository::class,
            \App\Repositories\Databases\UrlRepository::class
        );

        //Hash
        $this->app->bind(
            \App\Repositories\Contracts\HashRepository::class,
            \App\Repositories\Databases\HashRepository::class
        );
    }
}
