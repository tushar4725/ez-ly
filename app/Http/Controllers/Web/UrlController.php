<?php

namespace App\Http\Controllers\Web;

use App\Services\UrlService;
use App\Http\Controllers\Controller;

class UrlController extends Controller
{
	public function __construct(UrlService $urls)
	{
        $this->urls = $urls;
    }
    
    public function create()
    {
        $params = request()->all();

        $shortened_url = app()->call([$this->urls, 'create'], [
			'inputs'          => $params
        ]);
        
        return response()->success($shortened_url);
    }

    public function get($hash)
    {
        $shortened_url = $this->urls->getUrl($hash);

        return redirect()->to($shortened_url);
    }
}