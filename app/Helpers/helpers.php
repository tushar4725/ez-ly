<?php

use Carbon\Carbon;

/**
 * Check if user is logged-in.
 *
 * @return bool
 */
function isLoggedIn()
{
    return auth()->check();
}

/**
 * Get an instance of the logged-in user.
 *
 * @return \App\Models\User|null
 */
function getLoggedInUser()
{
    if(isLoggedIn()) {
        return auth()->user();
    }

    return null;
}

/**
 * Get the id of logged-in user.
 *
 * @return int
 */
function getLoggedInId()
{
    if(isLoggedIn()) {
            return auth()->user()->id;
    }
    return null;
}

/**
 * Get the class name from the full class path.
 *
 * @param  string $name
 * @return string
 */
function getIntendedUrl($default = '/')
{
    return session()->pull('url.intended', $default);
}

function carbon()
{
    return Carbon::now();
}

function getUrlHash($id)
{
    $hashDigits = [];
    $dividend = $id;
    $remainder = 0;
    
    while ($dividend > 0) {
        $remainder = $dividend%62; 
        $dividend = $dividend/62;
        if($remainder != 0){
            array_unshift($hashDigits, $remainder);
        }
    }
    $base62Alphabet = ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    $hashDigitsCount = count($hashDigits);
    $hashString = "";
    $i = 0;
    while ($hashDigitsCount > $i) {
        $hashString .= $base62Alphabet[$hashDigits[$i]];
        $i++;
    }
    return $hashString;
}

function getCompleteShortenedUrl($hash)
{
    return config('app.url') . "/{$hash}";
}
