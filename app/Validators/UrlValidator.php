<?php

namespace App\Validators;

class UrlValidator extends Validator
{
    /**
    * Validation rules.
    *
    * @param  string $type
    * @param  array $data
    * @return array
    */

    protected function rules($data, $type)
    {
        $rules = [];

        switch($type)
        {
            case 'create':
            case 'edit':
                $rules = [
                    'url'   => 'required|url|max:1000'
                ];
                break;
        }

        return $rules;
    }

    protected function messages($type)
    {
        return [
            //
        ];
    }
}