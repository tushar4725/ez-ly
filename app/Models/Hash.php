<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hash extends Model
{
    protected $table = 'hashs';
     /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Relationship
     * 
     */
    public function url()
    {
        return $this->belongsTo(Url::class);
    }

}