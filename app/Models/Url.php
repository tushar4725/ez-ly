<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
     /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Relationship
     * 
     */
    public function hash()
    {
        return $this->hasOne(Hash::class);
    }

}