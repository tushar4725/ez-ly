<?php

namespace App\Exceptions;

class DisabledAccountException extends \Exception
{
	/**
    * @var string
    */
    protected $message;

    /**
    * Create a new exception instance.
    *
    * @return void
    */
    function __construct($message = 'disabled account')
    {
        $this->message = $message;
    }
}